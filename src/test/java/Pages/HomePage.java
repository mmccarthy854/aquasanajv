package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class HomePage {

    public HomePage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    @FindBy(how = How.XPATH, using = "//button[contains(.,'Spa Days')]")
    public WebElement spaDayTab;

    @FindBy(how = How.ID, using = "spaVillageCode")
    public WebElement spaSelectDropdown;

    @FindBy(how = How.ID, using = "spaDayDatePicker")
    public WebElement datePicker;

    @FindBy(how = How.ID, using = "dayDifference")
    public WebElement flexibilityDropdown;

    @FindBy(how = How.NAME, using = "Login")
    public WebElement btnLogin;

    @FindBy(how = How.XPATH, using = "//button[contains(.,'Search now')]")
    public WebElement searchBtn;

    public void SelectSpaDays() {
        spaDayTab.click();
    }

    public void ChooseSpa(String spaName) {
        new Select(spaSelectDropdown).selectByVisibleText(spaName);
    }

    public void ChooseDate(Integer daysFromToday) {
        for (int i = 0; i < daysFromToday; i++) {
            datePicker.sendKeys("\uE014");
        }
        datePicker.sendKeys("Enter");
    }

    public void ChooseFlexibility(Integer dayRange) {
        String dayRangeString = String.valueOf(dayRange);
        new Select(flexibilityDropdown).selectByValue(dayRangeString);
    }

    public void SearchNow()
    {
        searchBtn.click();
    }

}
