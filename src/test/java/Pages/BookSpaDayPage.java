package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import java.util.List;

public class BookSpaDayPage {

    public BookSpaDayPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    @FindBy(how = How.CSS, using = "[id^='option-']")
    public List<WebElement> options;

    @FindBy(how = How.XPATH, using = "//a[contains(.,'Book Spa days')]")
    public WebElement bookSpaDayButton;

    @FindBy(how = How.XPATH, using = "//*[@id=\"0\"]/select")
    public WebElement guestDropdown;

    public void SelectDate(String occurance) {
        int optionId = 0;
        switch (occurance) {
            case ("first"):
                optionId = 0;
                break;
            case ("second"):
                optionId = 1;
                break;
            case ("third"):
                optionId = 2;
                break;
            case ("fourth"):
                optionId = 3;
                break;
            case ("fifth"):
                optionId = 4;
                break;
        }
        for (int i = 0; i < options.size(); i++) {
            if (i == optionId) {
                options.get(i).click();
            }
        }
    }

    public void SelectGuests(int numberOfGuests) {
        String guestNumString = String.valueOf(numberOfGuests);
        new Select(guestDropdown).selectByValue(guestNumString);
    }

    public void BookSpaDay() {
        bookSpaDayButton.click();
    }
}
