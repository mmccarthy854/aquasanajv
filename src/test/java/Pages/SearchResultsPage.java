package Pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class SearchResultsPage {

    public SearchResultsPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    // TODO - The below statements also return the articles with a class of 'is-hidden'
    // Look into return a list of elements containing more than one class
    @FindBy(how = How.XPATH, using = "//section[@class='b-search-results']/article[@class='b-product']")
    public List<WebElement> results;

    public boolean ManySearchResultsReturned() {
        return (results.size() > 0);
    }

    public void SelectResult(Integer occurance) {
        for (int i = 0; i < results.size(); i++) {
            if (i == (occurance - 1)) {
                results.get(i).findElement(By.xpath(".//div[@class='b-product__body']/div[@class='b-product__desc']/a")).click();
            }
        }
    }

}
