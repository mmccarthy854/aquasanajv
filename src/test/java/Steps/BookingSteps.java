package Steps;

import java.util.List;

import Base.BaseUtil;
import Pages.BookSpaDayPage;
import Pages.BookingConfirmationPage;
import Pages.HomePage;
import Pages.SearchResultsPage;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import cucumber.api.java.After;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class BookingSteps extends BaseUtil {

    private BaseUtil base;

    public BookingSteps(BaseUtil base) {
        this.base = base;
    }

    @Given("^I am on the Aquasana home page$")
    public void i_am_on_the_aquasana_home_page() {
        base.driver.navigate().to("https://www.aquasana.co.uk/");
    }


    @When("^I select Spa Days")
    public void i_select_spa_days() {
        HomePage page = new HomePage(base.driver);
        page.SelectSpaDays();
    }

    @When("^I select (.*) from the Choose Your Spa dropdown$")
    public void i_select_sherwood_forest_from_the_choose_your_spa_dropdown(String venue) {
        HomePage page = new HomePage(base.driver);
        page.ChooseSpa(venue);
    }

    @When("^I choose a day (\\d+) days from now in the Date dropdown$")
    public void i_choose_a_date_from_the_date_dropdown(int days) {
        HomePage page = new HomePage(base.driver);
        page.ChooseDate(days);
    }

    @When("^I set (\\d+) days on the Flexibility dropdown$")
    public void i_choose_days_from_the_flexibility_dropdown(int days) {
        HomePage page = new HomePage(base.driver);
        page.ChooseFlexibility(days);
    }

    @When("^I click Search Now$")
    public void i_click_search_now() {
        HomePage page = new HomePage(base.driver);
        page.SearchNow();
    }

    @Then("^I should see many search results$")
    public void i_should_see_many_search_results() {
        SearchResultsPage page = new SearchResultsPage(base.driver);
        page.ManySearchResultsReturned();
    }

    @When("^I select More Information on the number (\\d+) result$")
    public void i_select_more_information_on_the_first_result(int occurance) {
        SearchResultsPage page = new SearchResultsPage(base.driver);
        page.SelectResult(occurance);
    }

    @When("^I select the (.*) available date")
    public void i_select_the_nth_available_date(String occurance) {
        BookSpaDayPage page = new BookSpaDayPage(base.driver);
        page.SelectDate(occurance);
    }

    @When("^I choose (\\d+) guests$")
    public void i_choose_guests(int numberOfGuests) {
        BookSpaDayPage page = new BookSpaDayPage(base.driver);
        page.SelectGuests(numberOfGuests);
    }

    @When("^I book my selection")
    public void i_book_my_selection() {
        BookingConfirmationPage confPage = new BookingConfirmationPage(base.driver);
        BookSpaDayPage bookPage = new BookSpaDayPage(base.driver);

        bookPage.BookSpaDay();
        new WebDriverWait(base.driver, 10).until(ExpectedConditions.visibilityOf(confPage.payNowButton));
    }
}